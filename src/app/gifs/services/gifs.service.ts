import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Gif, SearchGifsResponse } from '../interface/gifs.interface';

@Injectable({
  providedIn: 'root'
})
export class GifsService {
  
  private _apikey     : string = 'QrlQQztk2X2f0kiUqhABE6OT8UZDQEYm';
  private _servicioUrl: string = 'https://api.giphy.com/v1/gifs'
  private _historial : string[] = [];

  public resultados: Gif[] = [];

  get historial() {
    this._historial = this._historial.splice(0,10)
    return [...this._historial];
  }

  constructor(private http: HttpClient) { 

    this._historial = JSON.parse(localStorage.getItem('Historial')!) || [] ;
    
    this.resultados = JSON.parse(localStorage.getItem('resultados')!) || [] ;
    
  }

  buscarGifs( query: string = '') {
    query = query.trim().toLocaleLowerCase();
    if(!this._historial.includes(query)){
      this._historial.unshift(query);
      this._historial = this._historial.splice(0,10);

      localStorage.setItem('Historial', JSON.stringify(this._historial))
      
    } 

    const params = new HttpParams()
      .set('api_key',this._apikey)
      .set('limit','10')
      .set('q',query)
      .set('api_key',this._apikey)
      .set('offset','0')
      .set('rating','g')
      .set('lang','es');

    this.http.get<SearchGifsResponse>(`${this._servicioUrl}/search`,{ params })
      .subscribe( ( res ) => {
        
        this.resultados = res.data;
        localStorage.setItem('resultados', JSON.stringify(this.resultados))
      })
  }

  
}
